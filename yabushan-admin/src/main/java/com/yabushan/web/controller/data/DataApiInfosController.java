package com.yabushan.web.controller.data;

import java.util.List;

import com.yabushan.common.annotation.DataScope;
import com.yabushan.system.service.IDataApiInfosService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.DataApiInfos;

import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 数据服务APIController
 *
 * @author yabushan
 * @date 2021-03-28
 */
@RestController
@RequestMapping("/system/infos")
public class DataApiInfosController extends BaseController
{
    @Autowired
    private IDataApiInfosService dataApiInfosService;

    /**
     * 查询数据服务API列表
     */
    @PreAuthorize("@ss.hasPermi('system:infos:list')")
    @GetMapping("/list")
    public TableDataInfo list(DataApiInfos dataApiInfos)
    {
        startPage();
        List<DataApiInfos> list = dataApiInfosService.selectDataApiInfosList(dataApiInfos);
        return getDataTable(list);
    }

    /**
     * 导出数据服务API列表
     */
    @PreAuthorize("@ss.hasPermi('system:infos:export')")
    @Log(title = "数据服务API", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(DataApiInfos dataApiInfos)
    {
        List<DataApiInfos> list = dataApiInfosService.selectDataApiInfosList(dataApiInfos);
        ExcelUtil<DataApiInfos> util = new ExcelUtil<DataApiInfos>(DataApiInfos.class);
        return util.exportExcel(list, "infos");
    }

    /**
     * 获取数据服务API详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:infos:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dataApiInfosService.selectDataApiInfosById(id));
    }

    /**
     * 新增数据服务API
     */
    @PreAuthorize("@ss.hasPermi('system:infos:add')")
    @Log(title = "数据服务API", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataApiInfos dataApiInfos)
    {
        return toAjax(dataApiInfosService.insertDataApiInfos(dataApiInfos));
    }

    /**
     * 修改数据服务API
     */
    @PreAuthorize("@ss.hasPermi('system:infos:edit')")
    @Log(title = "数据服务API", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataApiInfos dataApiInfos)
    {
        return toAjax(dataApiInfosService.updateDataApiInfos(dataApiInfos));
    }

    /**
     * 删除数据服务API
     */
    @PreAuthorize("@ss.hasPermi('system:infos:remove')")
    @Log(title = "数据服务API", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataApiInfosService.deleteDataApiInfosByIds(ids));
    }
}
