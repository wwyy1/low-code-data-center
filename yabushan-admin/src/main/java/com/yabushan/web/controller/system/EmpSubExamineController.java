package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubExamine;
import com.yabushan.system.service.IEmpSubExamineService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工考核情况子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/examine")
public class EmpSubExamineController extends BaseController
{
    @Autowired
    private IEmpSubExamineService empSubExamineService;

    /**
     * 查询员工考核情况子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:examine:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubExamine empSubExamine)
    {
        startPage();
        List<EmpSubExamine> list = empSubExamineService.selectEmpSubExamineList(empSubExamine);
        return getDataTable(list);
    }

    /**
     * 导出员工考核情况子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:examine:export')")
    @Log(title = "员工考核情况子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubExamine empSubExamine)
    {
        List<EmpSubExamine> list = empSubExamineService.selectEmpSubExamineList(empSubExamine);
        ExcelUtil<EmpSubExamine> util = new ExcelUtil<EmpSubExamine>(EmpSubExamine.class);
        return util.exportExcel(list, "examine");
    }

    /**
     * 获取员工考核情况子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:examine:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubExamineService.selectEmpSubExamineById(recId));
    }

    /**
     * 新增员工考核情况子集
     */
    @PreAuthorize("@ss.hasPermi('system:examine:add')")
    @Log(title = "员工考核情况子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubExamine empSubExamine)
    {
        return toAjax(empSubExamineService.insertEmpSubExamine(empSubExamine));
    }

    /**
     * 修改员工考核情况子集
     */
    @PreAuthorize("@ss.hasPermi('system:examine:edit')")
    @Log(title = "员工考核情况子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubExamine empSubExamine)
    {
        return toAjax(empSubExamineService.updateEmpSubExamine(empSubExamine));
    }

    /**
     * 删除员工考核情况子集
     */
    @PreAuthorize("@ss.hasPermi('system:examine:remove')")
    @Log(title = "员工考核情况子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubExamineService.deleteEmpSubExamineByIds(recIds));
    }
}
