package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubQualification;
import com.yabushan.system.service.IEmpSubQualificationService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工职业资格认证子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/qualification")
public class EmpSubQualificationController extends BaseController
{
    @Autowired
    private IEmpSubQualificationService empSubQualificationService;

    /**
     * 查询员工职业资格认证子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:qualification:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubQualification empSubQualification)
    {
        startPage();
        List<EmpSubQualification> list = empSubQualificationService.selectEmpSubQualificationList(empSubQualification);
        return getDataTable(list);
    }

    /**
     * 导出员工职业资格认证子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:qualification:export')")
    @Log(title = "员工职业资格认证子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubQualification empSubQualification)
    {
        List<EmpSubQualification> list = empSubQualificationService.selectEmpSubQualificationList(empSubQualification);
        ExcelUtil<EmpSubQualification> util = new ExcelUtil<EmpSubQualification>(EmpSubQualification.class);
        return util.exportExcel(list, "qualification");
    }

    /**
     * 获取员工职业资格认证子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:qualification:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubQualificationService.selectEmpSubQualificationById(recId));
    }

    /**
     * 新增员工职业资格认证子集
     */
    @PreAuthorize("@ss.hasPermi('system:qualification:add')")
    @Log(title = "员工职业资格认证子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubQualification empSubQualification)
    {
        return toAjax(empSubQualificationService.insertEmpSubQualification(empSubQualification));
    }

    /**
     * 修改员工职业资格认证子集
     */
    @PreAuthorize("@ss.hasPermi('system:qualification:edit')")
    @Log(title = "员工职业资格认证子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubQualification empSubQualification)
    {
        return toAjax(empSubQualificationService.updateEmpSubQualification(empSubQualification));
    }

    /**
     * 删除员工职业资格认证子集
     */
    @PreAuthorize("@ss.hasPermi('system:qualification:remove')")
    @Log(title = "员工职业资格认证子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubQualificationService.deleteEmpSubQualificationByIds(recIds));
    }
}
