package com.yabushan.form.controller;

import ch.qos.logback.core.util.ContextUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.core.page.TableDataInfo;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.common.utils.StringUtils;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.form.domain.FormData;
import com.yabushan.form.domain.FormInfos;
import com.yabushan.form.service.IFormInfosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * 自定义表单Controller
 *
 * @author yabushan
 * @date 2021-06-26
 */
@RestController
@RequestMapping("/form/api")
public class FormApiController extends BaseController
{


    @Autowired
    private IFormInfosService formInfosService;
    private Object o;

    @GetMapping(value = "/getOneInfo/{id}")
    public AjaxResult getOneInfo(@PathVariable("id") Long id)
    {
        FormInfos formInfos = formInfosService.selectFormInfosById(id);
        return AjaxResult.success(formInfos);
    }


    @PostMapping("/addinfos")
    public AjaxResult addInfo(@RequestBody FormInfos formInfo)
    {
        JSONObject object=JSONObject.parseObject(formInfo.getFields());
        FormInfos form = JSON.toJavaObject(object,FormInfos.class);
        form.setFormMemo(formInfo.getFormMemo());
        form.setFields(formInfo.getFields());
        form.setCreateBy(formInfo.getCreateBy());
        form.setCreateTime(new Date());

        return toAjax(formInfosService.insertFormInfos(form));
    }


    //插入数据
    @PostMapping("/insertRow")
    public AjaxResult insertRow(@RequestBody FormData data){
        Long formId=null;
        //校验表单是否存在
        if(StringUtils.isEmpty(data.getFormId())){
            return  AjaxResult.error("非法链接！");
        }

        if(StringUtils.isEmpty(data.getUid())){
            return  AjaxResult.error("非法链接！");
        }
        // 校验创建人是否存在
        formId = Long.valueOf(data.getFormId());
        FormInfos formInfos = formInfosService.selectFormInfosById(formId);
        if(formInfos==null){
            return  AjaxResult.error("非法链接！");
        }
        if(StringUtils.isEmpty(formInfos.getCreateBy()) || !formInfos.getCreateBy().equals(data.getUid())){
            return  AjaxResult.error("非法链接！");
        }
        //封装sql语句
        int keyLength = data.getKey().length;
        int valueLength = data.getValue().length;
        if(keyLength != valueLength){
            return  AjaxResult.error("表单字段和内容信息不正确！");
        }
        StringBuffer sqlbuf = new StringBuffer();
        sqlbuf.append("insert into  custom_"+data.getFormId() + "(");

        StringBuffer keybuf=new StringBuffer();
        StringBuffer valuebuf = new StringBuffer();

        for(int i=0;i<keyLength;i++){

            if(i==keyLength-1){
                keybuf.append(data.getKey()[i]);
                keybuf.append(") values (");
                valuebuf.append("'").append(data.getValue()[i]).append("'");
                valuebuf.append(")");
            }else {
                keybuf.append(data.getKey()[i]);
                keybuf.append(" , ");
                valuebuf.append("'").append(data.getValue()[i]).append("'").append(",");
            }
        }
        String sqlparam = sqlbuf.append(keybuf.toString()).append(valuebuf.toString()).toString();
        try {
            formInfosService.dynamicsInsert(sqlparam);
        }catch (Exception e){
            return AjaxResult.error(e.toString());
        }
        return toAjax(1);

    }



    @GetMapping("/list")
    public TableDataInfo list()
    {
        FormInfos formInfos  = new FormInfos();
        List<FormInfos> list = formInfosService.selectFormInfosList(formInfos);
        return getDataTable(list);
    }




}
