package com.yabushan.form.service.impl;

import java.util.List;
import com.yabushan.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.form.mapper.JimuReportDbMapper;
import com.yabushan.form.domain.JimuReportDb;
import com.yabushan.form.service.IJimuReportDbService;

/**
 * 报表APIService业务层处理
 *
 * @author yabushan
 * @date 2021-07-03
 */
@Service("yabushanddb")
public class JimuReportDbServiceImpl implements IJimuReportDbService
{
    @Autowired
    private JimuReportDbMapper jimuReportDbMapper;

    /**
     * 查询报表API
     *
     * @param id 报表APIID
     * @return 报表API
     */
    @Override
    public JimuReportDb selectJimuReportDbById(String id)
    {
        return jimuReportDbMapper.selectJimuReportDbById(id);
    }

    /**
     * 查询报表API列表
     *
     * @param jimuReportDb 报表API
     * @return 报表API
     */
    @Override
    public List<JimuReportDb> selectJimuReportDbList(JimuReportDb jimuReportDb)
    {
        return jimuReportDbMapper.selectJimuReportDbList(jimuReportDb);
    }

    /**
     * 新增报表API
     *
     * @param jimuReportDb 报表API
     * @return 结果
     */
    @Override
    public int insertJimuReportDb(JimuReportDb jimuReportDb)
    {
        jimuReportDb.setCreateTime(DateUtils.getNowDate());
        return jimuReportDbMapper.insertJimuReportDb(jimuReportDb);
    }

    /**
     * 修改报表API
     *
     * @param jimuReportDb 报表API
     * @return 结果
     */
    @Override
    public int updateJimuReportDb(JimuReportDb jimuReportDb)
    {
        jimuReportDb.setUpdateTime(DateUtils.getNowDate());
        return jimuReportDbMapper.updateJimuReportDb(jimuReportDb);
    }

    /**
     * 批量删除报表API
     *
     * @param ids 需要删除的报表APIID
     * @return 结果
     */
    @Override
    public int deleteJimuReportDbByIds(String[] ids)
    {
        return jimuReportDbMapper.deleteJimuReportDbByIds(ids);
    }

    /**
     * 删除报表API信息
     *
     * @param id 报表APIID
     * @return 结果
     */
    @Override
    public int deleteJimuReportDbById(String id)
    {
        return jimuReportDbMapper.deleteJimuReportDbById(id);
    }
}
