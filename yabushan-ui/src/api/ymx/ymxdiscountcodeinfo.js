import request from '@/utils/request'

// 查询折扣与保修信息列表
export function listYmxdiscountcodeinfo(query) {
  return request({
    url: '/ymx/ymxdiscountcodeinfo/list',
    method: 'get',
    params: query
  })
}

// 查询折扣与保修信息详细
export function getYmxdiscountcodeinfo(discountCodeId) {
  return request({
    url: '/ymx/ymxdiscountcodeinfo/' + discountCodeId,
    method: 'get'
  })
}

// 新增折扣与保修信息
export function addYmxdiscountcodeinfo(data) {
  return request({
    url: '/ymx/ymxdiscountcodeinfo',
    method: 'post',
    data: data
  })
}

// 修改折扣与保修信息
export function updateYmxdiscountcodeinfo(data) {
  return request({
    url: '/ymx/ymxdiscountcodeinfo',
    method: 'put',
    data: data
  })
}

// 删除折扣与保修信息
export function delYmxdiscountcodeinfo(discountCodeId) {
  return request({
    url: '/ymx/ymxdiscountcodeinfo/' + discountCodeId,
    method: 'delete'
  })
}

// 导出折扣与保修信息
export function exportYmxdiscountcodeinfo(query) {
  return request({
    url: '/ymx/ymxdiscountcodeinfo/export',
    method: 'get',
    params: query
  })
}