import request from '@/utils/request'

// 查询日志列表
export function listYmxloginfo(query) {
  return request({
    url: '/ymx/ymxloginfo/list',
    method: 'get',
    params: query
  })
}

// 查询日志详细
export function getYmxloginfo(logId) {
  return request({
    url: '/ymx/ymxloginfo/' + logId,
    method: 'get'
  })
}

// 新增日志
export function addYmxloginfo(data) {
  return request({
    url: '/ymx/ymxloginfo',
    method: 'post',
    data: data
  })
}

// 修改日志
export function updateYmxloginfo(data) {
  return request({
    url: '/ymx/ymxloginfo',
    method: 'put',
    data: data
  })
}

// 删除日志
export function delYmxloginfo(logId) {
  return request({
    url: '/ymx/ymxloginfo/' + logId,
    method: 'delete'
  })
}

// 导出日志
export function exportYmxloginfo(query) {
  return request({
    url: '/ymx/ymxloginfo/export',
    method: 'get',
    params: query
  })
}