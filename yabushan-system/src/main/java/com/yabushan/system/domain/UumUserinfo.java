package com.yabushan.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 租户用户对象 uum_userinfo
 *
 * @author yabushan
 * @date 2021-04-18
 */
public class UumUserinfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 员工ID */
    private String employeeId;

    /** 用户帐号 */
    @Excel(name = "用户帐号")
    private String loginId;

    /** 用户全名 */
    @Excel(name = "用户全名")
    private String fullName;

    /** 所属地区(租户)ID */
    @Excel(name = "所属地区(租户)ID")
    private String regionKey;

    /** 所属地区(租户)名称 */
    @Excel(name = "所属地区(租户)名称")
    private String regionName;

    /** 所属公司 */
    @Excel(name = "所属公司")
    private String companyName;

    /** 公司ID */
    @Excel(name = "公司ID")
    private String companyId;

    /** 分公司 */
    @Excel(name = "分公司")
    private String branchName;

    /** 分公司ID */
    @Excel(name = "分公司ID")
    private String branchId;

    /** 部门 */
    @Excel(name = "部门")
    private String departmentName;

    /** 部门序号 */
    @Excel(name = "部门序号")
    private String departmentId;

    /** 科室 */
    @Excel(name = "科室")
    private String officeName;

    /** 科室ID */
    @Excel(name = "科室ID")
    private String officeId;

    /** 团队 */
    @Excel(name = "团队")
    private String teamName;

    /** 团队序号 */
    @Excel(name = "团队序号")
    private String teamId;

    /** 小组 */
    @Excel(name = "小组")
    private String groupName;

    /** 小组序号 */
    @Excel(name = "小组序号")
    private String groupId;

    /** 工作组织ID */
    @Excel(name = "工作组织ID")
    private String workOrgId;

    /** 工作组织 */
    @Excel(name = "工作组织")
    private String workOrgName;

    /** 人员类型 */
    @Excel(name = "人员类型")
    private String jobType;

    /** 移动电话 */
    @Excel(name = "移动电话")
    private String telPhone;

    /** 办公邮件 */
    @Excel(name = "办公邮件")
    private String workEmail;

    /** 联系地址 */
    @Excel(name = "联系地址")
    private String addressInfo;

    /** 生日 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date userBirthday;

    /** 性别 */
    @Excel(name = "性别")
    private String sysUserSex;

    /** 职位 */
    @Excel(name = "职位")
    private String postTitle;

    /** 流程职务职级 */
    @Excel(name = "流程职务职级")
    private String employeeClass;

    /** 用户职位职级 */
    @Excel(name = "用户职位职级")
    private String userPostLevel;

    /** 部门内排序号 */
    @Excel(name = "部门内排序号")
    private Long orderId;

    /** 加入公司日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "加入公司日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date userJoinDate;

    /** 民族 */
    @Excel(name = "民族")
    private String userNation;

    /** 政治面貌 */
    @Excel(name = "政治面貌")
    private String userReligion;

    /** 离开公司日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离开公司日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date userQuitDate;

    /** 职务描述 */
    @Excel(name = "职务描述")
    private String dutyDesc;

    /** 账号状态 1:启用 0:停用 2限权 */
    @Excel(name = "账号状态 1:启用 0:停用 2限权")
    private String accountStatus;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 人员开始生效时间 */
    @Excel(name = "人员开始生效时间")
    private String empStartTime;

    /** 编制所在组织ID */
    @Excel(name = "编制所在组织ID")
    private String orgId;

    /** 编制所在组织名称 */
    @Excel(name = "编制所在组织名称")
    private String orgIdName;

    public void setEmployeeId(String employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getEmployeeId()
    {
        return employeeId;
    }
    public void setLoginId(String loginId)
    {
        this.loginId = loginId;
    }

    public String getLoginId()
    {
        return loginId;
    }
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getFullName()
    {
        return fullName;
    }
    public void setRegionKey(String regionKey)
    {
        this.regionKey = regionKey;
    }

    public String getRegionKey()
    {
        return regionKey;
    }
    public void setRegionName(String regionName)
    {
        this.regionName = regionName;
    }

    public String getRegionName()
    {
        return regionName;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getCompanyName()
    {
        return companyName;
    }
    public void setCompanyId(String companyId)
    {
        this.companyId = companyId;
    }

    public String getCompanyId()
    {
        return companyId;
    }
    public void setBranchName(String branchName)
    {
        this.branchName = branchName;
    }

    public String getBranchName()
    {
        return branchName;
    }
    public void setBranchId(String branchId)
    {
        this.branchId = branchId;
    }

    public String getBranchId()
    {
        return branchId;
    }
    public void setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;
    }

    public String getDepartmentName()
    {
        return departmentName;
    }
    public void setDepartmentId(String departmentId)
    {
        this.departmentId = departmentId;
    }

    public String getDepartmentId()
    {
        return departmentId;
    }
    public void setOfficeName(String officeName)
    {
        this.officeName = officeName;
    }

    public String getOfficeName()
    {
        return officeName;
    }
    public void setOfficeId(String officeId)
    {
        this.officeId = officeId;
    }

    public String getOfficeId()
    {
        return officeId;
    }
    public void setTeamName(String teamName)
    {
        this.teamName = teamName;
    }

    public String getTeamName()
    {
        return teamName;
    }
    public void setTeamId(String teamId)
    {
        this.teamId = teamId;
    }

    public String getTeamId()
    {
        return teamId;
    }
    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public String getGroupName()
    {
        return groupName;
    }
    public void setGroupId(String groupId)
    {
        this.groupId = groupId;
    }

    public String getGroupId()
    {
        return groupId;
    }
    public void setWorkOrgId(String workOrgId)
    {
        this.workOrgId = workOrgId;
    }

    public String getWorkOrgId()
    {
        return workOrgId;
    }
    public void setWorkOrgName(String workOrgName)
    {
        this.workOrgName = workOrgName;
    }

    public String getWorkOrgName()
    {
        return workOrgName;
    }
    public void setJobType(String jobType)
    {
        this.jobType = jobType;
    }

    public String getJobType()
    {
        return jobType;
    }
    public void setTelPhone(String telPhone)
    {
        this.telPhone = telPhone;
    }

    public String getTelPhone()
    {
        return telPhone;
    }
    public void setWorkEmail(String workEmail)
    {
        this.workEmail = workEmail;
    }

    public String getWorkEmail()
    {
        return workEmail;
    }
    public void setAddressInfo(String addressInfo)
    {
        this.addressInfo = addressInfo;
    }

    public String getAddressInfo()
    {
        return addressInfo;
    }
    public void setUserBirthday(Date userBirthday)
    {
        this.userBirthday = userBirthday;
    }

    public Date getUserBirthday()
    {
        return userBirthday;
    }
    public void setSysUserSex(String sysUserSex)
    {
        this.sysUserSex = sysUserSex;
    }

    public String getSysUserSex()
    {
        return sysUserSex;
    }
    public void setPostTitle(String postTitle)
    {
        this.postTitle = postTitle;
    }

    public String getPostTitle()
    {
        return postTitle;
    }
    public void setEmployeeClass(String employeeClass)
    {
        this.employeeClass = employeeClass;
    }

    public String getEmployeeClass()
    {
        return employeeClass;
    }
    public void setUserPostLevel(String userPostLevel)
    {
        this.userPostLevel = userPostLevel;
    }

    public String getUserPostLevel()
    {
        return userPostLevel;
    }
    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public Long getOrderId()
    {
        return orderId;
    }
    public void setUserJoinDate(Date userJoinDate)
    {
        this.userJoinDate = userJoinDate;
    }

    public Date getUserJoinDate()
    {
        return userJoinDate;
    }
    public void setUserNation(String userNation)
    {
        this.userNation = userNation;
    }

    public String getUserNation()
    {
        return userNation;
    }
    public void setUserReligion(String userReligion)
    {
        this.userReligion = userReligion;
    }

    public String getUserReligion()
    {
        return userReligion;
    }
    public void setUserQuitDate(Date userQuitDate)
    {
        this.userQuitDate = userQuitDate;
    }

    public Date getUserQuitDate()
    {
        return userQuitDate;
    }
    public void setDutyDesc(String dutyDesc)
    {
        this.dutyDesc = dutyDesc;
    }

    public String getDutyDesc()
    {
        return dutyDesc;
    }
    public void setAccountStatus(String accountStatus)
    {
        this.accountStatus = accountStatus;
    }

    public String getAccountStatus()
    {
        return accountStatus;
    }
    public void setIdCard(String idCard)
    {
        this.idCard = idCard;
    }

    public String getIdCard()
    {
        return idCard;
    }
    public void setEmpStartTime(String empStartTime)
    {
        this.empStartTime = empStartTime;
    }

    public String getEmpStartTime()
    {
        return empStartTime;
    }
    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getOrgId()
    {
        return orgId;
    }
    public void setOrgIdName(String orgIdName)
    {
        this.orgIdName = orgIdName;
    }

    public String getOrgIdName()
    {
        return orgIdName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("employeeId", getEmployeeId())
                .append("loginId", getLoginId())
                .append("fullName", getFullName())
                .append("regionKey", getRegionKey())
                .append("regionName", getRegionName())
                .append("companyName", getCompanyName())
                .append("companyId", getCompanyId())
                .append("branchName", getBranchName())
                .append("branchId", getBranchId())
                .append("departmentName", getDepartmentName())
                .append("departmentId", getDepartmentId())
                .append("officeName", getOfficeName())
                .append("officeId", getOfficeId())
                .append("teamName", getTeamName())
                .append("teamId", getTeamId())
                .append("groupName", getGroupName())
                .append("groupId", getGroupId())
                .append("workOrgId", getWorkOrgId())
                .append("workOrgName", getWorkOrgName())
                .append("jobType", getJobType())
                .append("telPhone", getTelPhone())
                .append("workEmail", getWorkEmail())
                .append("addressInfo", getAddressInfo())
                .append("userBirthday", getUserBirthday())
                .append("sysUserSex", getSysUserSex())
                .append("postTitle", getPostTitle())
                .append("employeeClass", getEmployeeClass())
                .append("userPostLevel", getUserPostLevel())
                .append("orderId", getOrderId())
                .append("userJoinDate", getUserJoinDate())
                .append("userNation", getUserNation())
                .append("userReligion", getUserReligion())
                .append("userQuitDate", getUserQuitDate())
                .append("dutyDesc", getDutyDesc())
                .append("accountStatus", getAccountStatus())
                .append("idCard", getIdCard())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("empStartTime", getEmpStartTime())
                .append("orgId", getOrgId())
                .append("orgIdName", getOrgIdName())
                .toString();
    }
}
