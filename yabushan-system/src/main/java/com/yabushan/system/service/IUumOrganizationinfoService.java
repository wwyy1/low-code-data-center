package com.yabushan.system.service;

import java.util.List;

import com.yabushan.system.domain.UumOrganizationinfo;
import com.yabushan.system.utils.OrgTreeSelect;
import com.yabushan.system.utils.UumOrgInfo;

/**
 * 租户组织Service接口
 *
 * @author yabushan
 * @date 2021-04-17
 */
public interface IUumOrganizationinfoService
{
    /**
     * 查询租户组织
     *
     * @param ouguid 租户组织ID
     * @return 租户组织
     */
    public UumOrganizationinfo selectUumOrganizationinfoById(String ouguid);

    /**
     * 查询租户组织列表
     *
     * @param uumOrganizationinfo 租户组织
     * @return 租户组织集合
     */
    public List<UumOrganizationinfo> selectUumOrganizationinfoList(UumOrganizationinfo uumOrganizationinfo);

    /**
     * 新增租户组织
     *
     * @param uumOrganizationinfo 租户组织
     * @return 结果
     */
    public int insertUumOrganizationinfo(UumOrganizationinfo uumOrganizationinfo);

    /**
     * 修改租户组织
     *
     * @param uumOrganizationinfo 租户组织
     * @return 结果
     */
    public int updateUumOrganizationinfo(UumOrganizationinfo uumOrganizationinfo);

    /**
     * 批量删除租户组织
     *
     * @param ouguids 需要删除的租户组织ID
     * @return 结果
     */
    public int deleteUumOrganizationinfoByIds(String[] ouguids);

    /**
     * 删除租户组织信息
     *
     * @param ouguid 租户组织ID
     * @return 结果
     */
    public int deleteUumOrganizationinfoById(String ouguid);

    /**
     * 查询租户组织列表
     *
     * @param uumOrgInfo 租户组织
     * @return 租户组织集合
     */
    public List<UumOrgInfo> selectUumOrgInfoList(UumOrgInfo uumOrgInfo);


    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    public List<OrgTreeSelect> buildDeptTreeSelect(List<UumOrgInfo> depts);
    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    public List<UumOrgInfo> buildDeptTree(List<UumOrgInfo> depts);
}
